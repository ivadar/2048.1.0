#include "core/game.h"
#include "core/tile.h"
#include <QFile>
#include <QTextStream>

Game::Game(int dimension) // конструктор основных элементов окна
{
    score = 0;
    bestscore = 0;
    board = new Board(dimension);
    restart();
}

Game::~Game()
{
    delete board;
}

void Game::move(Direction dir)
{
    // перемещение
    board->move(dir);

    // обновление score и bestscore
    if (board->isTileCollisionLastRound()){
        score += board->getPointsScoredLastRound();
    }
    if (bestscore < score)  {
           bestscore = score;
    }

    // если больше нет возможности сделать ход, игра окончена
    if (!board->movePossible())
        gameOver = true;

//    notifyObservers();
}

void Game::restart()
{
    board->reset();
    gameOver = false;
    score = 0;
}

// альтернатива (и более эффективная реализация):
// сохраняем переменную win в Board,
// каждый раз, когда плитки сталкиваются, проверяем, приводит ли обновление к значению WINNING_VALUE
bool Game::won() const
{
    for (int i = 0; i < board->getDimension(); ++i)
        for (int j = 0; j < board->getDimension(); ++j)
            if (board->getTile(i,j) != NULL && board->getTile(i,j)->getValue() == WINNING_VALUE)
                return true;

    return false;
}
