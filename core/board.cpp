#include "core/board.h"
#include "core/tile.h"

#include <iostream>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

#include <QDebug>

using namespace std;

Board::Board(int dimension)     // конструктор класса Board, передаем размер поля
{
    pointsScoredLastRound = 0;
    tileCollisionLastRound = false;
    this->dimension = dimension;
    init();
}

Board::Board(const Board &other)    // конструктор класса Board для создания уже частично заполненного поля, передаем другое поле
{
    dimension = other.dimension;
    init();
    for (int i = 0; i < dimension; ++i)
        for (int j = 0; j < dimension; ++j) {
            if (other.board[i][j] == NULL)
                board[i][j] = NULL;
            else
                board[i][j] = new Tile(*(other.board[i][j]));
        }
}

Board::~Board()     // деструктор плиток
{
    for (int i = 0 ; i < dimension; ++i)
        for (int j = 0; j < dimension; ++j)
            delete board[i][j];
}

void Board::init()      // создание пустого доски
{
    // устанавливаем размер массива, равному размеру доски
    board.resize(dimension);
    for (int i = 0; i < dimension; ++i)
        board[i].resize(dimension);
    // в цикле через доску создаем плитки
    for (int i = 0; i < dimension; ++i)
        for (int j = 0; j < dimension; ++j)
            board[i][j] = NULL;
}

QVector<int> Board::freePosition() // генерируем случайную свободную позицию на доске
{
    QVector<int> pos;
    if (full()) {
        pos.append(-1);
        pos.append(-1);
    } else {
        int i,j;
        do {
            i = rand() % dimension;
            j = rand() % dimension;
        } while (board[i][j] != NULL);
        pos.append(i);
        pos.append(j);
    }
    return pos;
}

bool Board::changed(Board& other) const // проверка изменений
{
    if (dimension != other.dimension)
        return false;
    for (int i = 0; i < dimension; i++)
        for (int j = 0; j < dimension; ++j)
            if ( ( (board[i][j] == NULL && other.board[i][j] != NULL) ||
                   (board[i][j] != NULL && other.board[i][j] == NULL) ) ||
                 ( (board[i][j] != NULL && other.board[i][j] != NULL) &&
                   board[i][j]->getValue() != other.board[i][j]->getValue()) )
                return true;
    return false;
}

void Board::reset()        // переведим доску в начальное состояние (все пусто, кроме двух 2 плиток)
{
    pointsScoredLastRound = 0;
    tileCollisionLastRound = false;

    for (int i = 0; i < dimension ; ++i) {
        for (int j = 0; j < dimension; ++j) {
            delete board[i][j];
            board[i][j] = NULL;
        }
    }

    QVector<int> start = freePosition();
    board[start[0]][start[1]] = new Tile();
    start = freePosition();
    board[start[0]][start[1]] = new Tile();

}

Tile* Board::getTile(int i, int j)      // получение плитки
{
    return board[i][j];
}

void Board::move(Direction direction)       // перемещение элементов
{
    Board pre_move_board(*this);

    prepareForNextMove();

    switch (direction) {
    case UP:
        for (int i = 0; i < dimension; ++i)
            for (int j = 0; j < dimension; ++j)
                moveVertically(i,j,UP);
        break;
    case DOWN:
        for (int i = dimension-1; i >= 0; --i)
            for (int j = 0; j < dimension; ++j)
                moveVertically(i,j,DOWN);
        break;
    case LEFT:
        for (int i = 0; i < dimension; ++i)
            for (int j = 0; j < dimension; ++j)
                moveHorizontally(i,j,LEFT);
        break;
    case RIGHT:
        for (int i = 0; i < dimension; ++i)
            for (int j = dimension-1; j >= 0; --j)
                moveHorizontally(i,j, RIGHT);
    }

    // если доска изменилась и столкновения плиток не было, поместить новую плитку
    if (changed(pre_move_board)) {
        QVector<int> newpos = freePosition();
        board[newpos[0]][newpos[1]] = new Tile();
    }

//    notifyObservers();
}

void Board::prepareForNextMove()    // метод, который проверяет каждую ячейку на возможность премещения
{
    tileCollisionLastRound = false;
    pointsScoredLastRound = 0;
    for (int i = 0; i < dimension; ++i) {
        for (int j = 0; j < dimension; ++j) {
            if (board[i][j] != nullptr) {
                board[i][j]->setUpgratedThisMove(false);
            }
        }
    }
}

bool Board::movePossible() const    // проверка возможности перемещения
{
    if (full()) {
        // если можно ли еще переместить
        Board newBoard(*this);
        newBoard.move(UP);
        if (changed(newBoard)) return true;
        newBoard.move(DOWN);
        if (changed(newBoard)) return true;
        newBoard.move(LEFT);
        if (changed(newBoard)) return true;
        newBoard.move(RIGHT);
        if (changed(newBoard)) return true;

        // нельзя переместить
        return false;
    }
    else {
        return true;
    }
}

void Board::moveHorizontally(int i, int j, Direction dir) // перемещение по горизонтали
{
    if (board[i][j] != NULL) {
        bool tileCollision = false;
        int newj;
        if (dir == RIGHT)
            newj = j + 1;
        // иначе влево
        else
            newj = j - 1;

        // продолжаем движение в направлении dir, пока мы не столкнемся с чем-то или не выйдем за пределы
        while (inbounds(i,newj) && board[i][newj] == NULL) {
            if (dir == RIGHT)
                newj++;
            else
                newj--;
        }

        // вне поля или ...
        if (!inbounds(i,newj)) {
            if (dir == RIGHT)
                board[i][dimension-1] = board[i][j];
            else
                board[i][0] = board[i][j];
        }
        // ... столкновение
        else {
            // столкновение с плиткой такого же номинала
            if (board[i][newj]->getValue() == board[i][j]->getValue() &&
                !board[i][newj]->getUpgratedThisMove()) {

                tileCollision = true;
                handleCollision(i, newj); // обрабатываем столкновение
            }
            // столкновение с плиткой с другим номиналом, оставляем эту плитку рядом
            else {
                if (dir == RIGHT)
                    board[i][newj-1] = board[i][j];
                else
                    board[i][newj+1] = board[i][j];
            }
        }
        // удаляем исходную плитку, если сделали несколько ходов
        // или если не сделали несколько ходов, а слились с плиткой, рядом с которой стояли
        if ( (dir == RIGHT && newj-1 != j) || (dir == LEFT && newj+1 != j) || tileCollision )
            board[i][j] = NULL;

        if (tileCollision)
            tileCollisionLastRound = true;
    }
}

void Board::moveVertically(int i, int j, Direction dir) // перемещение по горизонтали
{
    if (board[i][j] != NULL) {
        bool tileCollision = false;
        int newi;
        if (dir == UP)
            newi = i - 1;
        // вниз
        else
            newi = i + 1;

        // продолжаем движение в направлении dir, пока не столкнемся с чем-то или не выйдем за пределы
        while (inbounds(newi,j) && board[newi][j] == NULL) {
            if (dir == UP)
                newi--;
            else
                newi++;
        }

        // вне поля или ...
        if (!inbounds(newi,j)) {
            if (dir == UP)
                board[0][j] = board[i][j];
            else
                board[dimension-1][j] = board[i][j];
        }
        // ... столкновение
        else {
            // столкновение с плиткой такого же номинала
            if (board[newi][j]->getValue() == board[i][j]->getValue() &&
                !board[newi][j]->getUpgratedThisMove()) {
                tileCollision = true;
                handleCollision(newi, j); // обрабатываем столкновение
            }
            // столкновение с плиткой с другим номиналом, оставляем эту плитку рядом
            else {
                if (dir == UP)
                    board[newi+1][j] = board[i][j];
                else
                    board[newi-1][j] = board[i][j];
            }
        }
        // удаляем исходную плитку, если сделали несколько ходов
        // или если не сделали несколько ходов, а слились с плиткой, рядом с которой стояли
        if ( (dir == UP && newi+1 != i) || (dir == DOWN && newi-1 != i) || tileCollision )
            board[i][j] = NULL;

        if (tileCollision)
            tileCollisionLastRound = true;
    }

}

void Board::handleCollision(int i, int j) // обработка столкновения
{
    board[i][j]->upgrade(); // умножаем значение на 2
    board[i][j]->setUpgratedThisMove(true);
    pointsScoredLastRound += board[i][j]->getValue(); // увеличиваем значение счетчика
}

bool Board::full() const // проверка заполнена ли доска
{
    bool full = true;
    for (int i = 0; i < dimension; ++i)
        for (int j = 0; j < dimension; ++j)
            if (board[i][j] == NULL)
                full = false;
    return full;
}

bool Board::inbounds(int i, int j) // проверка в пределах ли доски
{
    return (i >= 0 && j >= 0 && i < dimension && j < dimension);
}
