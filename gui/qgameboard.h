#ifndef QGAMEBOARD_H
#define QGAMEBOARD_H

#include "core/observer.h"
#include "qgameoverwindow.h"

#include <QVector>
#include <QWidget>

class QResetButton;
class Game;
class QKeyEvent;
class QTile;
class QGridLayout;
class QVBoxLayout;
class QLabel;

class QGameBoard : public QWidget, public Observer
{
    Q_OBJECT
public:
    explicit QGameBoard(QWidget *parent = 0);
    ~QGameBoard();

    void notify();

private:

    Game* game;
    // графическое представление доски
    QVector<QVector<QTile*> > gui_board;
    // главный макет
    QVBoxLayout *mainLayout;
    // сеточный макет доски
    QGridLayout *boardLayout;
    // score
    QLabel *score;
    // bestscore
    QLabel *bestscore;
    // game over widget
    QGameOverWindow gameOverWindow;
    // winner widget
    QLabel *youwin;
    // кнопка новой игры
    QResetButton* reset;

    void drawBoard();

protected:
    void keyPressEvent(QKeyEvent *event);

signals:

public slots:
    void resetGame();

};

#endif // QGAMEBOARD_H
