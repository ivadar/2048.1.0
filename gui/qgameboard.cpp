#include "gui/qgameboard.h"
#include "core/board.h"
#include "core/game.h"
#include "gui/qtile.h"
#include "core/tile.h"
#include "gui/qresetbutton.h"
#include "mainwindow_2.h"

#include <QVBoxLayout>
#include <QGridLayout>
#include <QPushButton>
#include <QLabel>
#include <QKeyEvent>
#include <QVBoxLayout>
#include <QString>

#include <QDebug>

QGameBoard::~QGameBoard()
{
    delete game;
}

QGameBoard::QGameBoard(QWidget *parent) :
    QWidget(parent)
{
    // устанавливаем размер по умолчанию
    resize(500,500);

    // создаем основной макет
    mainLayout = new QVBoxLayout();
    setLayout(mainLayout);

    // будет создан в drawBoard ()
    boardLayout = NULL;

    // создание игры
    game = new Game(4);
//    game->registerObserver(this);

    // создание графичной доски и ее отрисовка
    gui_board.resize(game->getGameBoard()->getDimension());
    for (int i = 0; i < game->getGameBoard()->getDimension(); ++i)
        gui_board[i].resize(game->getGameBoard()->getDimension());
    for (int i = 0; i < game->getGameBoard()->getDimension(); ++i)
        for (int j = 0; j < game->getGameBoard()->getDimension(); ++j)
            gui_board[i][j] = NULL;
    drawBoard();

    // создание score и bestscore и добавление в окно
    score = new QLabel(QString("SCORE: %1").arg(game->getScore()));
    score->setStyleSheet("QLabel { color: rgb(235,224,214); font: 16pt; }");
    score->setFixedHeight(50);
    mainLayout->insertWidget(1, score, 0, Qt::AlignRight);

    bestscore = new QLabel(QString("BEST SCORE: %1").arg(game->getBestScore()));
    bestscore->setStyleSheet("QLabel { color: rgb(235,224,214); font: 16pt; }");
    bestscore->setFixedHeight(50);
    mainLayout->insertWidget(1, bestscore, 0, Qt::AlignRight);

    // создание кнопки для новой игры
    reset = new QResetButton(this);
    reset->setFixedHeight(50);
    mainLayout->insertWidget(1, reset, 0, Qt::AlignRight);

    // таблица стилей плиток
    setStyleSheet("QGameBoard { background-color: rgb(187,173,160) }");

    // связываем сигнал нажатия кноки новой игры и слот обновления игры
    connect(reset, SIGNAL(clicked()), this, SLOT(resetGame()));

    connect(gameOverWindow.getResetBtn(), SIGNAL(clicked()), this, SLOT(resetGame()));
}

void QGameBoard::keyPressEvent(QKeyEvent *event) // нажатие кнопок и вызов соответствующего метода
{
    switch (event->key()) {
    case Qt::Key_Up:
        game->move(UP);
        break;
    case Qt::Key_Left:
        game->move(LEFT);
        break;
    case Qt::Key_Right:
        game->move(RIGHT);
        break;
    case Qt::Key_Down:
        game->move(DOWN);
        break;
    }
}

void QGameBoard::notify() // варианты проигрыша или победы
{
    if (game->isGameOver())
        gameOverWindow.show();

    if (game->won())
        score->setText(QString("You hit 2048, congratulations! Keep playing to increase your score.\t\t SCORE: %1").arg(game->getScore()));
    else
        score->setText(QString("SCORE: %1").arg(game->getScore()));

    drawBoard();
}

void QGameBoard::drawBoard() // отрисовка доски
{
    delete boardLayout;
    boardLayout = new QGridLayout();
    for (int i = 0; i < game->getGameBoard()->getDimension(); ++i) {
        for (int j = 0; j < game->getGameBoard()->getDimension(); ++j) {
            delete gui_board[i][j];
            gui_board[i][j] = new QTile(game->getGameBoard()->getTile(i,j));
            boardLayout->addWidget(gui_board[i][j], i, j);
            gui_board[i][j]->draw();
        }
    }
    mainLayout->insertLayout(0, boardLayout);
}


void QGameBoard::resetGame() // обновление игры
{
    game->restart();
    drawBoard();
    score->setText(QString("SCORE: %1").arg(game->getScore()));
    gameOverWindow.hide();
}
