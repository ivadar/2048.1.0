#include "mainwindow_2.h"
#include "ui_mainwindow_2.h"
#include "dialog.h"
#include "result.h"
#include <QMessageBox>
#include <iostream>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include "core/board.h"
#include "gui/qgameboard.h"

#include "gui/qgameoverwindow.h"


MainWindow_2::MainWindow_2(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow_2)
{
    ui->setupUi(this);
}

MainWindow_2::~MainWindow_2()
{
    delete ui;
}

void MainWindow_2::on_actionAbout_triggered()
{
    QMessageBox:: information(this, "About", "Курсовую работу выполнила: Иванченко Дарья Владимировна. Группа: М8О-110Б-20.");

}


void MainWindow_2::on_actionRules_triggered()
{
    QMessageBox:: information(this, "Rules", "2048 — это игра, где отлично воссоединились традиционный тетрис и пятнашки."
"Стандартное игровое поле имеет форму квадрата 4x4. Вы можете выбрать размер поля (по умолчанию стоит 4х4). "
"В начале игры вам выдается два кирпичика с цифрой «2», нажимая кнопку вверх, вправо, влево или вниз, все ваши кирпичики будут смещаться в ту же сторону. "
"Но, при соприкосновении клеточек с одним и тем же номиналом, они объединяются и создают сумму вдвое больше. "
"Игра заканчивается тогда, когда все пустые ячейки заполнены, и вы больше не можете передвигать клеточки ни в одну из сторон или когда на одном из кубов, появилась заветная цифра 2048. "
"Удачи!");
}


void MainWindow_2::on_actionQuit_triggered()
{
     QApplication::quit();
}


void MainWindow_2::on_pushButton_clicked()
{
    int selectedSize = ui->comboBox->currentText().toInt();
    gameBoard = new QGameBoard();
    setCentralWidget(gameBoard);
//    srand(time(NULL));
//    QGameBoard board;
//    gameBoard->show();
//    srand(time(NULL));
//    QGameBoard gameboard;
//    gameboard.show();
}


void MainWindow_2::on_pushButton_2_clicked()
{
    Dialog what_your_name;
    what_your_name.setModal(true);
    what_your_name.exec();
}


void MainWindow_2::on_pushButton_3_clicked()
{
    Result result;
    result.setModal(true);
    result.exec();
}

